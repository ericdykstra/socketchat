# README

Welcome to the Socketchat app, a Ruby on Rails application that leverages Websockets via ActionCable for a real-time chat experience.

## Installation

This app requires you to have `Ruby` and `Bundler` installed.
Specifically, this application uses Ruby version `2.6.1`. If you don't have Ruby installed already, I recommend using [asdf version manager](https://github.com/asdf-vm/asdf).

Once Ruby is installed, you can run `gem install bundler` to install Bundler, and then run `bundle install` in the application folder to install dependencies.

Socketchat relies on Redis to handle the websocket data transmission, so we will need to also have redis installed. Read how [here](https://redis.io/topics/quickstart) if it's not installed already

After your dependencies are installed, you need to create and migrate your database, which you can do with the command `rails db:create db:migrate`

## Running the application

`bundle exec rails server` will run the application

The redis server needs to be run separately, which can be done with `redis-server`.

To seed some initital data, run `rails db:seed`

## Dependencies

Dependencies on a standard Rails installation that we are not using are Turbolinks, Coffeescript, and Spring.

Aside from the standard dependencies of Rails, we are using:

 - Devise - A plug-and-play solution for authentication, highly customizable and extensible
 - haml - HTML templating that is less awkward to write, and more readable than erb
 - Bulma - A light, flex-box based CSS solution
 - JQuery - Our transforms are simple, and JQuery helps make our code a little more readable
 - color-generator - For rolling initial custom colors for chat
