Rails.application.routes.draw do
  root controller: :messages, action: :index

  resources :messages
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
