require "rails_helper"

describe MessagesHelper do
  describe "#handle_message" do

    context "standard message" do
      it "returns standardmessage hash" do
        test_message = "test message"
        assert_equal({ body: test_message }, helper.handle_message(test_message))
      end
    end

    context "direct message" do
      it "returns dm hash" do
        test_user = User.create!(email: "test@example.com", username: "test_user", password: "hunter2")
        dm_contents = "this is a dm"
        test_message = "/dm test_user #{dm_contents}"

        assert_equal({
          body: dm_contents,
          receiving_user: test_user,
        }, helper.handle_message(test_message))
      end
    end

    context "invalid message" do
      it "returns nil" do
        test_message = "/this_is_not_a_command at all"

        assert_nil helper.handle_message(test_message)
      end
    end
  end
end
