class MessagesController < ApplicationController
  include MessagesHelper

  before_action :load_message, only: [:destroy]

  MESSAGE_DISPLAY_DEFAULT = 20

  def index
    @messages = Message.viewable(current_user.id).last(MESSAGE_DISPLAY_DEFAULT)
  end

  def create
    message_data = handle_message(message_body)

    if message_data
      @message = Message.create!(
        {user: current_user}.merge(message_data)
      )

      ChatChannel.broadcast_to "chat", @message
    end
  end

  def destroy
    if current_user.admin? || current_user == @message.user
      DestroyChannel.broadcast_to "destroy", @message
      @message.destroy(current_user)
    end
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end

  def message_body
    message_params["body"]
  end

  def load_message
    @message = Message.find params[:id]
  end
end
