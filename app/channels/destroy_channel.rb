class DestroyChannel < ApplicationCable::Channel
  def subscribed
    stream_for "destroy"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
