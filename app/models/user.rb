class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :messages

  after_create :set_color

  private

  def set_color
    update color: ColorGenerator.new(lightness: 0.3, seed: id).create_hex
  end
end
