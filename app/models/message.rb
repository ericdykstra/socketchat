class Message < ApplicationRecord
  belongs_to :user
  belongs_to :deleted_by_user, class_name: 'User', optional: true
  belongs_to :receiving_user, class_name: 'User', optional: true

  delegate :username, :color, to: :user

  scope :viewable, ->(user_id) { where(receiving_user_id: [nil, user_id]).or(where(user_id: user_id)).not_deleted }

  scope :not_deleted, -> { where(deleted_by_user_id: nil) }

  def destroy(user)
    update!(deleted_by_user: user)
  end

  def can_delete?(user)
    user.admin? || self.user == user
  end

  def as_json(options)
    super(options).merge(username: username, color: color, receiving_username: receiving_user&.username)
  end

  def dm?
    ! receiving_user_id.nil?
  end
end
