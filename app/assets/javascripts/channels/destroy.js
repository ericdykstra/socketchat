App.destroy = App.cable.subscriptions.create("DestroyChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    $("#msg-id-" + data['id']).html('')
  }
});
