App.chat = App.cable.subscriptions.create("ChatChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {
    var new_comment = $('#message-template').children().clone(true, true);

    new_comment.find('#message-username').text(data['username'])
    new_comment.find('#message-username').attr('style', "color: #" + data['color'])
    new_comment.find('#message-body').text(data['body'])
    new_comment.prevObject.attr('id', "msg-id-" + data['id'])
    new_comment.find('a').attr('href', '/messages/' + data['id'])

    if (data['receiving_username'] !== null) {
      new_comment.find('#message-recipient').text(" → " + data['receiving_username'])
    }

    $('.chat-window').append(new_comment)
    document.getElementById("new_message").reset()
  }
});
