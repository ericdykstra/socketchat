module MessagesHelper
  def handle_message(message_body)
    if message_body[0..2] == "/dm"
      _, username, *dm_text = message_body.split(" ")
      dm_text = dm_text.join(" ")
      {
        receiving_user: User.find_by(username: username),
        body: dm_text
      }
    elsif message_body[0] != "/"
      { body: message_body }
    else
      nil
    end
  end
end
