# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

1.upto(4).each do |i|
  user = User.create!(email: "test#{i}@example.com", username: "test_user#{i}", password: "hunter2", admin: true)
  1.upto(2).each do |j|
    user.messages.create!(body: "test message ##{j} from user #{i}")
  end
end
