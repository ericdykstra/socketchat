class AddColorToUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :color, :string, default: "000000"

    User.find_each {|u| u.send(:set_color)}
  end

  def down
    remove_column :users, :color
  end
end
