class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :user, foreign_key: true
      t.references :deleted_by_user, foreign_key: { to_table: :users }
      t.text :body, default: '', null: false

      t.timestamps
    end
  end
end
